<?php
/***********************************************************
 * 自定义
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\home\model;
use think\facade\Db;
class CustomModel
{
    // 获取单条记录
    public function getInfo($aid, $field = '', $isshowbody = true){
        $data = array();
        if (!empty($field)) {
            $field_arr = explode(',', $field);
            foreach ($field_arr as $key => $val) {
                $val = trim($val);
                if (preg_match('/^([a-z]+)\./i', $val) == 0) {
                    array_push($data, 'a.'.$val);
                } else {
                    array_push($data, $val);
                }
            }
            $field = implode(',', $data);
        }
        $result = [];
        if ($isshowbody) {
            $field = !empty($field) ? $field : 'b.*, a.*';
            $result = Db::name('archives')
                    ->field($field)
                    ->alias('a')
                    ->join('custommodel_content b', 'b.aid = a.aid', 'LEFT')
                    ->find($aid);
        } else {
            $field = !empty($field) ? $field : 'a.*';
            $result = Db::name('archives')
                    ->field($field)
                    ->alias('a')
                    ->where('a.aid',$aid)
                    ->find();
        }
        return $result;
    }
}