
<p align="center">
    <img src="/readme/images/logo.png" style="width:100px;"/>
</p>
<h1 align="center"> pcfcms企业建站系统</h1> 
<p align="center">    
    <b>如果对您有帮助，您可以点右上角 "Star" 支持一下 谢谢！</b>
</p>

## 项目介绍
   pcfcms是基于TP6.0框架为核心开发的免费+开源的企业内容管理系统，专注企业建站用户需求提供海量各行业模板，降低中小企业网站建设、网络营销成本，致力于打造用户舒适的建站体验

### 导航栏目
[使用手册](http://www.pcfcms.com/help/kaifawendang.html)
 | [安装说明](/readme/安装说明.md)
 | [官网地址](http://www.pcfcms.com)
 | [服务器](https://www.aliyun.com/minisite/goods?userCode=m9b66lzd)
 | [授权价格](http://www.pcfcms.com)
- - -

####   :fire:  演示站后台:[<a href='http://demo.pcfcms.com' target="_blank"> 查看 </a>]       
<a href='http://demo.pcfcms.com/login.php' target="_blank">http://demo.pcfcms.com/login.php</a>  账号：admin  密码：123456



### PCFCMS推荐阿里云服务器配置

阿里云领取2000元代金劵：[<a href="https://www.aliyun.com/minisite/goods?userCode=m9b66lzd">https://www.aliyun.com</a>]
 
### 页面展示
![输入图片说明](https://images.gitee.com/uploads/images/2020/0606/141023_622ad2be_306081.png "管理控制台.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0606/141036_d758f52f_306081.png "定时任务.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0606/141050_8ca012b9_306081.png "节点管理.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0606/141100_1379006f_306081.png "留言.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0606/141114_2c7d2e00_306081.png "模型.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0606/141128_1a88149b_306081.png "权限管理.png")
![![输入图片说明](https://images.gitee.com/uploads/images/2020/0606/141148_024b68d4_306081.png "信息.png")](https://images.gitee.com/uploads/images/2020/0606/141138_727f7104_306081.png "数据库备份.png")


# 特别鸣谢
感谢以下的项目,排名不分先后

ThinkPHP：http://www.thinkphp.cn

Bootstrap：http://getbootstrap.com

layui: https://www.layui.com/

### 版权信息
本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2020-2021 by PCFCMS (http://www.pcfcms.com)

All rights reserved。