<?php
/***********************************************************
 * 日志记录
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Session;
class AdminLog extends Common
{
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list = Db::name('admin_log')
              ->alias('a')
              ->join('admin b','a.admin_id = b.admin_id')
              ->field("a.*, b.user_name,b.pen_name")
              ->where($tableWhere['where'])
              ->order($tableWhere['order'])
              ->paginate($limit)->toArray();
        $pcfdata = $list['data'];
        $newdata = [];
        foreach ($pcfdata as $key => $value) {
            $newdata[$key]['log_id'] = $value['log_id'];
            $newdata[$key]['user_name'] = $value['user_name'];
            $newdata[$key]['pen_name'] = $value['pen_name'];
            $newdata[$key]['log_info'] = $value['log_info'];
            $newdata[$key]['log_ip'] = $value['log_ip'];
            $newdata[$key]['log_osName'] = $value['log_osName'];
            $newdata[$key]['log_device'] = $value['log_device'];
            $newdata[$key]['log_browserType'] = $value['log_browserType'];
            $newdata[$key]['log_time'] = pcftime($value['log_time']);
        }
        $result = ['code' => 0, 'msg' => 'ok','count' =>$list['total'],'data' => $newdata];
        return $result;
    }
    protected function pcftableWhere($post)
    {
        $where = [];
        if (isset($post['user_name']) && !empty($post['user_name'])) {
            $where[] = ['b.user_name', '=', $post['user_name']];
        }
        if (isset($post['startDate']) && !empty($post['startDate'])) {
            $where[] = ['a.log_time', '>=', strtotime($post['startDate'])];
        }
        if (isset($post['endDate']) && !empty($post['endDate'])) {
            $where[] = ['a.log_time', '<=', strtotime($post['endDate'])];
        }
        if(Session::get('admin_id') != 1 && Session::get('admin_info.role_id') > 0){
            $where[] = ['a.admin_id', '=', Session::get('admin_id')];
        }
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = "log_id DESC";
        return $result;
    }
}
