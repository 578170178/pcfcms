<?php
/***********************************************************
 * 单页模型
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Session;
use think\facade\Cache;
use think\facade\Request;
use app\admin\logic\FieldLogic;
class Single
{
    
    // 后置操作方法
    public function afterSave($aid, $post)
    {
        $fieldLogic = new FieldLogic();
        $post['aid'] = $aid;
        $addonFieldExt = !empty($post['addonFieldExt']) ? $post['addonFieldExt'] : [];
        $fieldLogic->PcfChannelPostData(6, $post, $addonFieldExt);
    }

    // 删除的后置操作方法
    public function afterDel($typeidArr = [])
    {
        if (is_string($typeidArr)) {
            $typeidArr = explode(',', $typeidArr);
        }
        // 同时删除文档表
        Db::name('archives')->where('typeid','IN', $typeidArr)->delete();
        // 同时删除内容表
        Db::name('single_content')->where('typeid','IN', $typeidArr)->delete();
        // 清除缓存
        Cache::delete("arctype");
        Cache::delete('admin_archives_release');
    }

}