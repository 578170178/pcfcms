<?php
/***********************************************************
 * 模型内容列表
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\admin\model;
use think\facade\Db;
use think\facade\Session;
use think\facade\Cache;
use think\facade\Request;
use app\common\model\Arctype;
class Content
{
    // 列表
    public function tableData($post)
    {
        if(isset($post['limit'])){
            $limit = $post['limit'];
        }else{
            $limit = 10;
        }
        $tableWhere = $this->pcftableWhere($post);
        $list1 = DB::name('archives')
               ->field("aid")
               ->where($tableWhere['where'])
               ->order($tableWhere['order'])
               ->paginate($limit);
        $data = $list1->getCollection()->toArray();
        $list = array_combine(array_column($data, 'aid'), $data);
        if ($list) {
            $aids = array_keys($list);
            $fields = "b.*, a.*, a.aid as aid";
            $row = DB::name('archives')
                    ->field($fields)
                    ->alias('a')
                    ->join('arctype b', 'a.typeid = b.id', 'LEFT')
                    ->where('a.aid', 'IN', $aids)
                    ->column($fields, 'aid');
            foreach ($list as $key => $val) {
                $row[$val['aid']]['arcurl'] = get_arcurl($row[$val['aid']]);
                $row[$val['aid']]['update_time'] = date('Y-m-d H:i:s', $row[$val['aid']]['update_time']);
                $row[$val['aid']]['add_time'] = date('Y-m-d H:i:s', $row[$val['aid']]['add_time']);
                $row[$val['aid']]['litpic'] = get_default_pic($row[$val['aid']]['litpic']);
                $list[$key] = $row[$key];
            }
            $list = array_merge($list);
        }
        $result = ['code' => 0, 'msg' => 'ok','count' =>$list1->total(),'data' => $list];
        return $result;
    }
    
    // 查询条件
    protected function pcftableWhere($post)
    {
        $where = [];
        // 应用搜索条件
        foreach (['keywords','typeid','sontypeid'] as $key) {
            if (isset($post[$key]) && !empty($post[$key])) {
                if ($key == 'keywords') {
                    $where[] = ['title','LIKE',"%{$post[$key]}%"];
                }elseif($key == 'typeid') {
                    $typeid = $post[$key];
                    $Arctype = new Arctype();
                    $hasRow = $Arctype->getHasChildren($typeid,true,true);
                    $typeids = get_arr_column($hasRow, 'id');
                    $where[] = ['typeid',"IN",$typeids];
                }elseif($key == 'sontypeid' && !isset($post['typeid'])) {
                    $typeid = $post['sontypeid'];
                    $Arctype = new Arctype();
                    $hasRow = $Arctype->getHasChildren($typeid,true,true);
                    $typeids = get_arr_column($hasRow, 'id');
                    $where[] = ['typeid',"IN",$typeids];
                }
            }
        }
        $where[] = ['channel','=',$post['channel']];
        $admin_info = session::get('admin_info');
        if (0 < intval($admin_info['role_id'])) {
            $auth_role_info = $admin_info['auth_role_info'];
            if(!empty($auth_role_info)){
                if(isset($auth_role_info['only_oneself']) && (1 == $auth_role_info['only_oneself'])){
                    $where[]= ['admin_id','=',$admin_info['admin_id']];
                }
            }
        }
        $result['where'] = $where;
        $result['field'] = "*";
        $result['order'] = "sort_order asc,aid desc";
        return $result;
    }

}