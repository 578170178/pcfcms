<?php
/***********************************************************
 * 字段绑定频道模型
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
namespace app\common\model;
use think\facade\Db;
use think\facade\Cache;
class Channelfield
{

    // 获取单条记录
    public function getInfo($id, $field = '*')
    {
        $result = Db::name('channelfield')->field($field)->where('id',$id)->find();
        return $result;
    }

    // 默认模型字段列表
    public function getListByWhere($map = [], $field = '*', $index_key = '')
    {
        $result = Db::name('channelfield')->field($field)
                ->where($map)
                ->order('sort_order asc, channel_id desc, id desc')
                ->select()
                ->toArray();
        if (!empty($index_key)) {
            $result = convert_arr_key($result, $index_key);
        }
        return $result;
    }    

    // 前端taglib_获取单条内容记录
    public function getInfoByWhere($where, $field = '*')
    {
        $result = Db::name('channelfield')
                    ->field($field)
                    ->where($where)
                    ->cache(true,ADMIN_CACHE_TIME,"channelfield")
                    ->find();
        return $result;
    }


}