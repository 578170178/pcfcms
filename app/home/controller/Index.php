<?php
/***********************************************************
 * 首页
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 20121年01月01日
***********************************************************/
namespace app\home\controller;
use think\facade\Db;
use think\facade\Request;
use think\facade\Session;
class Index extends Base
{

    public function _initialize() {
        parent::_initialize();
    }

    public function index() {
        return $this->fetch(":index");
    }

}