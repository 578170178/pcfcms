<?php

// 设置前台URL模式
if (!function_exists('set_home_url_mode')) 
{
    function set_home_url_mode() {
        $seo_pseudo = sysConfig('seo.seo_pseudo');
        if ($seo_pseudo == 1) {
            config('route.url_common_param', true);
            config('route.url_route_must', false);
        } elseif ($seo_pseudo == 2) {
            config('route.url_common_param', false);
            config('route.url_route_must', true);
        }
    }
}

// 设置栏目标题
if (!function_exists('set_typeseotitle')) 
{
    function set_typeseotitle($typename = '', $seo_title = '')
    {
        if (empty($seo_title)) {
            $web_name = sysConfig('web.web_title');
            $seo_liststitle_format = sysConfig('seo.seo_liststitle_format');
            switch ($seo_liststitle_format) 
            {
                case '1':
                    $seo_title = $typename.'_'.$web_name;
                    break;
                case '2':
                default:
                    $page = input('param.page/d', 1);
                    if ($page > 1) {
                        $typename .= "_第{$page}页";
                    }
                    $seo_title = $typename.'_'.$web_name;
                    break;
            }
        }
        return $seo_title;
    }
}

// 设置内容标题
if (!function_exists('set_arcseotitle')) 
{
    function set_arcseotitle($title = '', $seo_title = '', $typename = '')
    {
        if (empty($seo_title)) {
            static $web_name = null;
            null === $web_name && $web_name = sysConfig('web.web_title');
            static $seo_viewtitle_format = null;
            null === $seo_viewtitle_format && $seo_viewtitle_format = sysConfig('seo.seo_viewtitle_format');
            switch ($seo_viewtitle_format) 
            {
                case '1':
                    $seo_title = $title;
                    break;
                case '3':
                    $seo_title = $title.'_'.$typename.'_'.$web_name;
                    break;
                case '2':
                default:
                    $seo_title = $title.'_'.$web_name;
                    break;
            }
        }
        return $seo_title;
    }
}

/**
*  时间转化日期格式
* @param  string  $format 日期格式
* @param  intval  $t      时间戳
*/
if (!function_exists('MyDate')) 
{
    function MyDate($format = 'Y-m-d', $t = '')
    {
        if (!empty($t)) {$t = date($format, $t);}
        return $t;
    }
}

/**
*  时间转化日期格式
* @param  string  $format     日期格式
* @param  intval  $posttime   时间戳
*/
if (!function_exists('home_time_ago')) 
{
	function home_time_ago($format = 'Y-m-d', $posttime = '')
	{
		//当前时间的时间戳
		$nowtimes = time();
		//相差时间戳
		$counttime = $nowtimes - $posttime;
		//进行时间转换
		if ($counttime <= 60) {
			return '刚刚';
		} else if ($counttime > 60 && $counttime <= 120) {
			return '1分钟前';
		} else if ($counttime > 120 && $counttime <= 180) {
			return '2分钟前';
		} else if ($counttime > 180 && $counttime < 3600) {
			return intval(($counttime / 60)) . '分钟前';
		} else if ($counttime >= 3600 && $counttime < 3600 * 24) {
			return intval(($counttime / 3600)) . '小时前';
		} else if ($counttime >= 3600 * 24 && $counttime < 3600 * 24 * 2) {
			return '昨天';
		} else if ($counttime >= 3600 * 24 * 2 && $counttime < 3600 * 24 * 3) {
			return '前天';
		} else if ($counttime >= 3600 * 24 * 3 && $counttime <= 3600 * 24 * 7) {
			return intval(($counttime / (3600 * 24))) . '天前';
		} else if ($counttime >= 3600 * 24 * 7 && $counttime <= 3600 * 24 * 30) {
			return intval(($counttime / (3600 * 24 * 7))) . '周前';
		} else if ($counttime >= 3600 * 24 * 30 && $counttime <= 3600 * 24 * 365) {
			return intval(($counttime / (3600 * 24 * 30))) . '个月前';
		} else if ($counttime >= 3600 * 24 * 365) {
			return intval(($counttime / (3600 * 24 * 365))) . '年前';
		}
	}
}

/**
* 截取内容清除html之后的字符串长度，支持中文和其他编码
* @param string $str 需要转换的字符串
* @param string $start 开始位置
* @param string $length 截取长度
* @param string $suffix 截断显示字符
* @param string $charset 编码格式
*/
if (!function_exists('html_msubstr')) 
{
    function html_msubstr($str='', $start=0, $length=NULL, $suffix=false, $charset="utf-8") {
        $str = gzpcf_htmlspecialchars_decode($str);
        $str = checkStrHtml($str);
        return pcf_msubstr($str, $start, $length, $suffix, $charset);
    }
}

/**
* 自定义只针对htmlspecialchars编码过的字符串进行解码
* @param string $str 需要转换的字符串
* @param string $start 开始位置
* @param string $length 截取长度
* @param string $suffix 截断显示字符
* @param string $charset 编码格式
*/
if (!function_exists('gzpcf_htmlspecialchars_decode')) 
{
    function gzpcf_htmlspecialchars_decode($str='') {
        if (is_string($str) && stripos($str, '&lt;') !== false && stripos($str, '&gt;') !== false) {
            $str = htmlspecialchars_decode($str);
        }
        return $str;
    }
}

// 计算不同会员等级价格
if (!function_exists('home_price')) 
{
    function home_price($price,$level)
    {
        if ($level == 1) {
            return round($price*0.7);
        } else if ($level == 2) {
            return round($price*0.5);
        } 
    }
}

// 验证身份证号码
if (!function_exists('is_idcard')) 
{
    function is_idcard($id)
    {
          $id = strtoupper($id);
          $regx = "/(^\d{15}$)|(^\d{17}([0-9]|X)$)/";
          $arr_split = array();
          if(!preg_match($regx, $id))
          {
            return false;
          }
          if(15 == strlen($id)) //检查15位
          {
                $regx = "/^(\d{6})+(\d{2})+(\d{2})+(\d{2})+(\d{3})$/";
                @preg_match($regx, $id, $arr_split);
                //检查生日日期是否正确
                $dtm_birth = "19".$arr_split[2] . '/' . $arr_split[3]. '/' .$arr_split[4];
                if(!strtotime($dtm_birth))
                {
                  return false;
                }
                else
                {
                  return true;
                }
          }
          else //检查18位
          {
                $regx = "/^(\d{6})+(\d{4})+(\d{2})+(\d{2})+(\d{3})([0-9]|X)$/";
                @preg_match($regx, $id, $arr_split);
                $dtm_birth = $arr_split[2] . '/' . $arr_split[3]. '/' .$arr_split[4];
                if(!strtotime($dtm_birth)) //检查生日日期是否正确
                {
                  return false;
                }
                else
                {
                  //检验18位身份证的校验码是否正确。
                  //校验位按照ISO 7064:1983.MOD 11-2的规定生成，X可以认为是数字10。
                  $arr_int = array(7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2);
                  $arr_ch = array('1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2');
                  $sign = 0;
                  for ( $i = 0; $i < 17; $i++ )
                  {
                    $b = (int) $id{$i};
                    $w = $arr_int[$i];
                    $sign += $b * $w;
                  }
                  $n = $sign % 11;
                  $val_num = $arr_ch[$n];
                  if ($val_num != substr($id,17, 1))
                  {
                    return false;
                  }
                  else
                  {
                    return true;
                  }
                }
          }
    }
}