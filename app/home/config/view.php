<?php
/***********************************************************
 * 模板设置
 * @作者 pcfcms <1131680521@qq.com>
 * @主页 http://www.pcfcms.com
 * @时间 2021年01月01日
***********************************************************/
use think\facade\Request;
//第一步判断选择了那个模板
$theme = config('pcfcms.admin_config.tpl_theme');//默认模板
if(request::isMobile() && file_exists(WWW_ROOT."public/template/{$theme}/mobile")) {
    $themeTitle = "mobile/";
} else {
    $themeTitle = "pc/";
}
if(empty(PUBLIC_ROOT)){
    return [
        // 模板后缀
        'view_suffix'  => 'html',
        // 模板路径
        'view_path'    => './template/'.$theme.'/'.$themeTitle,
        // 视图输出字符串内容替换
        'tpl_replace_string'   => [
            '{__JS_PATH}' =>  request::domain().'/template/'.$theme.'/'.$themeTitle.'skin/js', 
            '{__IMG_PATH}' =>  request::domain().'/template/'.$theme.'/'.$themeTitle.'skin/img', 
            '{__CSS_PATH}'  =>  request::domain().'/template/'.$theme.'/'.$themeTitle.'skin/css', 
            '{__PUBLIC_PATH}'  => request::domain().'/template/'.$theme.'/'.$themeTitle.'skin',   
            '{__COMMON_PATH}' => request::domain().'/common', 
            '{__ADMIN_PATH}' => request::domain().'/admin',
        ]
    ];     
}else{
    return [
        // 模板后缀
        'view_suffix'  => 'html',
        // 模板路径
        'view_path'    => './'.PUBLIC_ROOT.'/template/'.$theme.'/'.$themeTitle,
        // 视图输出字符串内容替换
        'tpl_replace_string'   => [
            '{__JS_PATH}' =>  request::domain().'/'.PUBLIC_ROOT.'/template/'.$theme.'/'.$themeTitle.'skin/js', 
            '{__IMG_PATH}' =>  request::domain().'/'.PUBLIC_ROOT.'/template/'.$theme.'/'.$themeTitle.'skin/img', 
            '{__CSS_PATH}'  =>  request::domain().'/'.PUBLIC_ROOT.'/template/'.$theme.'/'.$themeTitle.'skin/css', 
            '{__PUBLIC_PATH}'  => request::domain().'/'.PUBLIC_ROOT.'/template/'.$theme.'/'.$themeTitle.'skin',   
            '{__COMMON_PATH}' => request::domain().'/'.PUBLIC_ROOT.'/common', 
            '{__ADMIN_PATH}' => request::domain().'/'.PUBLIC_ROOT.'/admin',
        ]
    ]; 
}
